// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SalesforceSDKCore",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SalesforceSDKCore",
            targets: ["SalesforceSDKCoreProxyTarget"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(name: "SalesforceAnalytics", url: "https://MatthewM_4859@bitbucket.org/MatthewM_4859/salesforce-analytics-spm", from: "1.1.4")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(name: "SalesforceSDKCore",
                      path: "./Sources/SalesforceSDKCore.xcframework"),
        .target(name: "SalesforceSDKCoreProxyTarget",
                dependencies: ["SalesforceAnalytics", "SalesforceSDKCore"],
                path: "./Sources/Proxy"
        )
    ]
)
