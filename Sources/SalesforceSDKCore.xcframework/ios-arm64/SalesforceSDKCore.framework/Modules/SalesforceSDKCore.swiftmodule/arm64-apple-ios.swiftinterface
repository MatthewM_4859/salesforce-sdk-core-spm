// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4.2 (swiftlang-1205.0.28.2 clang-1205.0.19.57)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name SalesforceSDKCore
import Combine
import CryptoKit
import Foundation
import SalesforceSDKCommon
@_exported import SalesforceSDKCore
import Swift
import SwiftUI
import UIKit
@_inheritsConvenienceInitializers @objc(SFSDKKeyValueEncryptedFileStoreViewController) public class KeyValueEncryptedFileStoreViewController : ObjectiveC.NSObject {
  @objc public func createUI() -> UIKit.UIViewController
  @objc override dynamic public init()
  @objc deinit
}
public enum RestClientError : Swift.Error {
  case apiResponseIsEmpty
  case apiFailed(response: Any?, underlyingError: Swift.Error, urlResponse: Foundation.URLResponse?)
  case decodingFailed(underlyingError: Swift.Error)
  case jsonSerialization(underlyingError: Swift.Error)
}
public struct RestResponse {
  public var urlResponse: Foundation.URLResponse {
    get
  }
  public init(data: Foundation.Data, urlResponse: Foundation.URLResponse)
  public func asJson() throws -> Any
  public func asData() -> Foundation.Data
  public func asString() -> Swift.String
  public func asDecodable<T>(type: T.Type, decoder: Foundation.JSONDecoder = .init()) throws -> T where T : Swift.Decodable
}
extension RestRequest {
  public var isQueryRequest: Swift.Bool {
    get
  }
}
extension RestClient {
  public struct QueryResponse<Record> : Swift.Decodable where Record : Swift.Decodable {
    public init(from decoder: Swift.Decoder) throws
  }
  public func send(request: SalesforceSDKCore.RestRequest, _ completionBlock: @escaping (Swift.Result<SalesforceSDKCore.RestResponse, SalesforceSDKCore.RestClientError>) -> Swift.Void)
  public func send(compositeRequest: SalesforceSDKCore.CompositeRequest, _ completionBlock: @escaping (Swift.Result<SalesforceSDKCore.CompositeResponse, SalesforceSDKCore.RestClientError>) -> Swift.Void)
  public func send(batchRequest: SalesforceSDKCore.BatchRequest, _ completionBlock: @escaping (Swift.Result<SalesforceSDKCore.BatchResponse, SalesforceSDKCore.RestClientError>) -> Swift.Void)
  public func fetchRecords<Record>(ofModelType modelType: Record.Type, forRequest request: SalesforceSDKCore.RestRequest, withDecoder decoder: Foundation.JSONDecoder = .init(), _ completionBlock: @escaping (Swift.Result<SalesforceSDKCore.RestClient.QueryResponse<Record>, SalesforceSDKCore.RestClientError>) -> Swift.Void) where Record : Swift.Decodable
  public func fetchRecords<Record>(ofModelType modelType: Record.Type, forQuery query: Swift.String, withApiVersion version: Swift.String = SFRestDefaultAPIVersion, withDecoder decoder: Foundation.JSONDecoder = .init(), _ completionBlock: @escaping (Swift.Result<SalesforceSDKCore.RestClient.QueryResponse<Record>, SalesforceSDKCore.RestClientError>) -> Swift.Void) where Record : Swift.Decodable
}
extension RestClient {
  public func publisher(for request: SalesforceSDKCore.RestRequest) -> Combine.Future<SalesforceSDKCore.RestResponse, SalesforceSDKCore.RestClientError>
  public func publisher(for request: SalesforceSDKCore.CompositeRequest) -> Combine.Future<SalesforceSDKCore.CompositeResponse, SalesforceSDKCore.RestClientError>
  public func publisher(for request: SalesforceSDKCore.BatchRequest) -> Combine.Future<SalesforceSDKCore.BatchResponse, SalesforceSDKCore.RestClientError>
  public func records<Record>(forRequest request: SalesforceSDKCore.RestRequest, withDecoder decoder: Foundation.JSONDecoder = .init()) -> Combine.AnyPublisher<SalesforceSDKCore.RestClient.QueryResponse<Record>, Swift.Never> where Record : Swift.Decodable
  public func records<Record>(forQuery query: Swift.String, withApiVersion version: Swift.String = SFRestDefaultAPIVersion, withDecoder decoder: Foundation.JSONDecoder = .init()) -> Combine.AnyPublisher<SalesforceSDKCore.RestClient.QueryResponse<Record>, Swift.Never> where Record : Swift.Decodable
}
@objc(SFSDKKeyValueEncryptedFileStore) public class KeyValueEncryptedFileStore : ObjectiveC.NSObject {
  @objc(storeDirectory) final public let directory: Foundation.URL
  @objc(storeName) final public let name: Swift.String
  @objc public var storeVersion: Swift.Int {
    get
  }
  @objc public static let maxStoreNameLength: Swift.Int
  @objc public init?(parentDirectory: Swift.String, name: Swift.String, encryptionKey: SalesforceSDKCore.SFEncryptionKey)
  @objc(sharedStoreWithName:) public static func shared(withName name: Swift.String) -> SalesforceSDKCore.KeyValueEncryptedFileStore?
  @objc(sharedStoreWithName:user:) public static func shared(withName name: Swift.String, forUserAccount user: SalesforceSDKCore.UserAccount) -> SalesforceSDKCore.KeyValueEncryptedFileStore?
  @objc(sharedGlobalStoreWithName:) public static func sharedGlobal(withName name: Swift.String) -> SalesforceSDKCore.KeyValueEncryptedFileStore?
  @objc(allStoreNames) public static func allNames() -> [Swift.String]
  @objc(allStoreNamesForUser:) public static func allNames(forUserAccount user: SalesforceSDKCore.UserAccount) -> [Swift.String]
  @objc(allGlobalStoreNames) public static func allGlobalNames() -> [Swift.String]
  @objc(removeSharedStoreWithName:) public static func removeShared(withName name: Swift.String)
  @objc(removeSharedStoreWithName:forUser:) public static func removeShared(withName name: Swift.String, forUserAccount user: SalesforceSDKCore.UserAccount)
  @objc(removeSharedGlobalStoreWithName:) public static func removeSharedGlobal(withName name: Swift.String)
  @objc(removeAllStores) public static func removeAllForCurrentUser()
  @objc(removeAllStoresForUser:) public static func removeAll(forUserAccount user: SalesforceSDKCore.UserAccount)
  @objc(removeAllGlobalStores) public static func removeAllGlobal()
  @objc(isValidStoreName:) public static func isValidName(_ name: Swift.String) -> Swift.Bool
  @discardableResult
  @objc public func saveValue(_ value: Swift.String, forKey key: Swift.String) -> Swift.Bool
  @objc public subscript(key: Swift.String) -> Swift.String? {
    @objc get
    @objc set
  }
  @discardableResult
  @objc public func removeValue(forKey key: Swift.String) -> Swift.Bool
  @objc public func removeAll()
  @objc public func allKeys() -> [Swift.String]?
  @objc public func count() -> Swift.Int
  @objc public func isEmpty() -> Swift.Bool
  @objc override dynamic public init()
  @objc deinit
}
public enum PushNotificationManagerError : Swift.Error {
  case registrationFailed
  case currentUserNotDetected
  public static func == (a: SalesforceSDKCore.PushNotificationManagerError, b: SalesforceSDKCore.PushNotificationManagerError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension PushNotificationManager {
  public func registerForSalesforceNotifications(_ completionBlock: @escaping (Swift.Result<Swift.Bool, SalesforceSDKCore.PushNotificationManagerError>) -> ())
  public func registerForSalesforceNotifications(user: SalesforceSDKCore.UserAccount, completionBlock: @escaping (Swift.Result<Swift.Bool, SalesforceSDKCore.PushNotificationManagerError>) -> ())
  public func unregisterForSalesforceNotifications(_ completionBlock: @escaping (Swift.Bool) -> ())
  public func unregisterForSalesforceNotifications(user: SalesforceSDKCore.UserAccount, _ completionBlock: @escaping (Swift.Bool) -> ())
}
public enum UserAccountManagerError : Swift.Error {
  case loginFailed(underlyingError: Swift.Error, authInfo: SalesforceSDKCore.AuthInfo)
  case loginJWTFailed(underlyingError: Swift.Error, authInfo: SalesforceSDKCore.AuthInfo)
  case refreshFailed(underlyingError: Swift.Error, authInfo: SalesforceSDKCore.AuthInfo)
  case userSwitchFailed(underlyingError: Swift.Error)
  case userSwitchFailedUnknown
}
extension UserAccountManager {
  public func login(_ completionBlock: @escaping (Swift.Result<(SalesforceSDKCore.UserAccount, SalesforceSDKCore.AuthInfo), SalesforceSDKCore.UserAccountManagerError>) -> Swift.Void) -> Swift.Bool
  public func login(using jwt: Swift.String, _ completionBlock: @escaping (Swift.Result<(SalesforceSDKCore.UserAccount, SalesforceSDKCore.AuthInfo), SalesforceSDKCore.UserAccountManagerError>) -> Swift.Void) -> Swift.Bool
  public func refresh(credentials: SalesforceSDKCore.OAuthCredentials, _ completionBlock: @escaping (Swift.Result<(SalesforceSDKCore.UserAccount, SalesforceSDKCore.AuthInfo), SalesforceSDKCore.UserAccountManagerError>) -> Swift.Void) -> Swift.Bool
  public func switchToNewUserAccount(_ completionBlock: @escaping (Swift.Result<SalesforceSDKCore.UserAccount, SalesforceSDKCore.UserAccountManagerError>) -> Swift.Void)
}
@_inheritsConvenienceInitializers @objc(SFSDKEncryptor) public class Encryptor : ObjectiveC.NSObject {
  @available(swift, obsoleted: 1.0)
  @objc public static func encrypt(data: Foundation.Data, using key: Foundation.Data) throws -> Foundation.Data
  public static func encrypt(data: Foundation.Data, using key: CryptoKit.SymmetricKey) throws -> Foundation.Data
  @available(swift, obsoleted: 1.0)
  @objc public static func decrypt(data: Foundation.Data, using key: Foundation.Data) throws -> Foundation.Data
  public static func decrypt(data: Foundation.Data, using key: CryptoKit.SymmetricKey) throws -> Foundation.Data
  @objc override dynamic public init()
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(SFSDKKeyGenerator) public class KeyGenerator : ObjectiveC.NSObject {
  @available(swift, obsoleted: 1.0)
  @objc public static func encryptionKey(for label: Swift.String) throws -> Foundation.Data
  public static func encryptionKey(for label: Swift.String) throws -> CryptoKit.SymmetricKey
  @objc override dynamic public init()
  @objc deinit
}
extension SalesforceSDKCore.PushNotificationManagerError : Swift.Equatable {}
extension SalesforceSDKCore.PushNotificationManagerError : Swift.Hashable {}
